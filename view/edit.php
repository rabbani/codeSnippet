<?php session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\Category\Category;
use \App\Utility\Utility;

$obj=new Category();
$theCategory=$obj-> edit($_GET['id']);

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Edit Category</title>
</head>
<body>
<h1>Edit an Item</h1>
<form action="update.php" method="post">
    <fieldset>
        <legend>
            Edit Category
        </legend>
        <input type="hidden" name="id" value="<?php echo $theCategory->id;?>"/>
        <div>
            <label for="categoryTitle"> Category Title</label>
            <input
                type="text" name="categoryTitle" id="categoryTitle" required="required" tabindex="3" value="<?php echo $theCategory->title;?>"/>
        </div>
        <div>
            <label for="categoryDescription"> Category Description</label>
            <input
                type="text" name="categoryDescription" id="categoryDescription" required="required" tabindex="3" value="<?php echo $theCategory->descriptoin ;?>"/>
        </div>
        <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
<a href="index.php">Back to the list</a>
</body>
</html>