<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\Category\Category;
use App\Message\Message;

$obj=new Category();
$_categories=$obj->index();
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>List of Category</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h1>List of Category</h1>

<div class="warning">
    <?php
    error_reporting(0);
    echo Message::flash();
    ?>

</div>

<div>
    <span>Search /Filter </span>
    <span id="utility">Download as PDF | XL <a href="create.php">Create New</a> </span>
    <select>
        <option>10</option>
        <option>20</option>
        <option>30</option>
        <option>40</option>
        <option>50</option>
    </select>
</div>
<table border="1">
    <thead>
    <tr>
        <th>Sl</th>
        <th>Category Title</th>
        <th>Category Description &dArr;</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php

    $slno=0;
    foreach($_categories as $category):
        $slno++;
        ?>

        <tr>
            <td><?php echo  $slno;?></td>
            <td><a href="view.php?id=<?php echo $category['id'];?>"><?php echo $category['title'];?></a></td>
            <td><?php echo $category['descriptoin'];?></td>
            <td><a href="view.php?id=<?php echo $category['id'];?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $category['id'];?>">Edit</a></td>
            <td>
                <form action="delete.php" method="post">
                    <input type="hidden" name="id" value="<?php echo $category['id'];?>">
                    <button type="submit">Delete</button>
                </form>
            </td>

<?php endforeach;?>

    </tbody>

</table>
<div><span>prev 1 | 2 | 3 next </span></div>
<div><span>Trash/Recover | Email To Friend</span></div>
</body>
</html>